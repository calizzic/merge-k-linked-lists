# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

import heapq

class Solution(object):
    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        loop through, find min of first element of each list, add min to list and reload
        Could expidite by finding min, knowing second smallest, and going till at second smallest. Not going to reduce worst case though. 
        Could also expidite by auto jumping to the second smallest
        
        Maintaining a minheap of first elements?
        -inserting into heap is log(n), better than o(n) finding the first element of all
        """
        heap = []
        for i in range(len(lists)):
            if lists[i]:
                heapq.heappush(heap,(lists[i].val,i))
        
        if(len(heap) == 0):
            return None
        
        min = heapq.heappop(heap)
        rootNode = ListNode(min[0])
        currentNode = rootNode
        
        next = lists[min[1]].next
        lists[min[1]] = next
        if next:
            heapq.heappush(heap, (lists[min[1]].val,min[1]))
        
        while len(heap)>0:
            min = heapq.heappop(heap)
            currentNode.next = ListNode(min[0])
            currentNode = currentNode.next
            
            next = lists[min[1]].next
            lists[min[1]] = next
            if next:
                heapq.heappush(heap, (lists[min[1]].val,min[1]))
        
        return rootNode
            
                
        
        
        
        
        